
const SHOPPING_CART = 'puffpass-shoppingCart'

export function getShoppingCart() {
  if(localStorage.getItem(SHOPPING_CART))
    return JSON.parse(localStorage.getItem(SHOPPING_CART));
  return []
}

function clearShoppingCart() {
  localStorage.removeItem(SHOPPING_CART);
}

export function setShoppingCart(cart) {
  localStorage.setItem(SHOPPING_CART, cart);
}
