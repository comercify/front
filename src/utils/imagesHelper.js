const PREFIX = "assets/"

export const addPrefix = () => require.context('../assets/', false, /\.png$ || \.jpg$/)
