import {getToken} from './auth'
import axios from 'axios'
import {API_URL, AUTHORIZED_HEADER, HEADER} from '../config/requestConfig'

const config = {
  headers: HEADER,
  onDownloadProgress: progressEvent => {
    let percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
  }
}

const authorizedHeader =
  {
    headers: AUTHORIZED_HEADER,
    onDownloadProgress: progressEvent => {
      let percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
    },

  }

const RequestHelper = {
  mountGetRequest(path){
    return axios.get(API_URL + path,config)
  },
  mountPostRequest(path,data){
    return axios.post(API_URL + path,data,config)
  },
  mountAuthorizedPostRequest(path,data){
    return axios.post(API_URL + path, data, authorizedHeader)
  },

  calcPrecoPrazo(cep){
    return this.mountAuthorizedPostRequest('/correio/calc-preco-prazo',cep)
  },
  getCheckoutsByUserId(id){
    return this.mountGetRequest('/user/get-checkouts-by-user-id/'+id)
  },
  createUser(user){
    return this.mountPostRequest('/user/create-user',user,config)
  },
  updateUserById(id,user){
    return this.mountAuthorizedPostRequest('/user/update-user-by-id/'+id,user)
  },
  getProductById(id){
    return this.mountGetRequest('/product/get-product-by-id/'+id)
  },
  getAllProducts(){
    return this.mountGetRequest('/product/get-all-products',config)
  }
}

export default RequestHelper


