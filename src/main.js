// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router/index'
import store from './store/index'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueMask from 'v-mask'
import SimpleVueValidation from 'simple-vue-validator';

Vue.use(SimpleVueValidation);

Vue.use(VueAxios, axios)
Vue.use(VueMask);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
