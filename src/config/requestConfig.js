import {getToken} from './../utils/auth'

export const API_URL = "https://obscure-spire-15457.herokuapp.com"

// export const API_URL = "http://52.15.127.32:3000"

export const HEADER = {
  'Content-Type': 'application/json',
}


export const AUTHORIZED_HEADER =  {
  'Content-Type': 'application/json',
  'Authorization': getToken()
}



