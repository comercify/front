
import Home from '@/views/Profile'

export const cog = {
  path: '/meu-perfil',
  name: 'profile',
  isActive : false,
  meta : {
    label: 'Meu perfil',
    auth: true,
    navItem: false,
    cogItem: true,
    icon: 'fa fa-home'
  },
  component: Home
}
