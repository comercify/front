import Vue from 'vue'

import {cog} from './cogMenu'

import Router from 'vue-router'
import Home from '@/views/Home'
import Store from '@/views/Store'
import ProductDetails from '@/views/ProductDetails'
import Checkout from '@/views/Checkout'
import Login from '@/views/Login'
import Sale from '@/views/Sale'
import Atendimento from '@/views/Atendimento'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      isActive : true,
      meta : {
        label: 'Home',
        navItem: true,
        auth: false,
        icon: 'fa fa-home'
      },
      component: Home
    },
    {
      path: '/loja',
      name: 'loja',
      isActive : true,
      meta : {
        label: 'Loja',
        navItem: true,
        auth: false,
        icon: 'fa fa-home'
      },
      component: Store,


    },
    {
      path: '/loja/:id',
      component: ProductDetails,
      meta:{
        navItem: false,
        cogItem: false

      }
    },
    {
      path: '/liquidacao',
      name: 'sale',
      isActive : true,
      meta : {
        label: 'Liquidação',
        navItem: true,
        auth: false,
        icon: 'fa fa-home'
      },
      component: Sale
    },
    {
      path: '/atendimento',
      name: 'Atendimento',
      isActive : true,
      meta : {
        label: 'Atendimento',
        navItem: true,
        auth: false,
        icon: 'fa fa-home'
      },
      component: Atendimento
    },

    {
      path: '/checkout/:id',
      name: 'checkout',
      isActive : false,
      meta : {
        label: '',
        navItem: false,
        icon: 'fa fa-home'
      },
      component: Checkout
    },
    {
      path: '/login',
      name: 'login',
      isActive : false,
      meta : {
        label: '',
        navItem: false,
        icon: 'fa fa-home'
      },
      component: Login
    },
    cog,

  ]
})
