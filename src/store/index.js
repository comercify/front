import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'

/*
 * modules
 */
import sidebar from './modules/sidebar'
import modal from './modules/modal'
import cart from './modules/cart'
import checkout from './modules/checkout'
import products from './modules/product'
import login from './modules/login'
import user from './modules/user'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  actions,
  getters,
  modules: {
    sidebar,
    checkout,
    modal,
    cart,
    products,
    login,
    user
  },
  strict: debug
})
