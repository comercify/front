import * as types from '../mutations'
import requestHelper from '../../utils/requestHelper'
import cart from './cart'

// initial state
const state = {
  all: [],
  pending: false
}

// getters
const getters = {
  allProducts: state => state.all,
  productPendingStatus: state=>state.pending
}

// actions
const actions = {
  getAllProducts ({ commit }) {
    commit(types.TOGGLE_PENDING_STATE,true)
    state.pending = true,
    requestHelper.getAllProducts().then(response=>{
      commit(types.TOGGLE_PENDING_STATE,false)
      const products = response.data.data
      commit(types.RECEIVE_PRODUCTS,{products})
      commit(types.SET_CART_INITIAL_STATE)
    },(err)=>{
      console.log(err)
    })
  }
}

// mutations
const mutations = {
  [types.RECEIVE_PRODUCTS] (state, { products }) {
    state.all = products
  },
  [types.TOGGLE_PENDING_STATE] (state,  status ) {
    state.pending = status
  },

  [types.ADD_TO_CART] (state, { id }) {
    state.all[id].quantity--
  },
  [types.REMOVE_FROM_CART] (state, { id }) {
    state.all[id].quantity++
  },

}

export default {
  state,
  getters,
  actions,
  mutations
}
