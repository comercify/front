import * as types from '../mutations'
import * as shoppingCart from '../../utils/shoppingCart'
import product from './product'

// initial state
// shape: [{ id, quantity }]
const state = {
  added: [],
}

// getters
const getters = {

}

// mutations
const mutations = {
  [types.ADD_TO_CART] (state, { id }) {
    const record = state.added.find(product => product.id === id)

    if (!record) {
      state.added.push({
        id: id,
        qtd: 1
      })
    } else
      record.qtd++

    const cartJson = JSON.stringify(state.added)
    shoppingCart.setShoppingCart(cartJson)
  },

  [types.REMOVE_FROM_CART] (state, { id }) {
    const record = state.added.find(product => product.id === id)

    if (record.qtd === 1) {
      const index = state.added.findIndex(added => added.id === id)
      state.added.splice(index, 1)
    } else
      record.qtd--
    const cartJson = JSON.stringify(state.added)
    shoppingCart.setShoppingCart(cartJson)
  },

  [types.SET_CART_INITIAL_STATE](state){
    const cart = shoppingCart.getShoppingCart()
    cart.map(cartProduct=>{
      const id = cartProduct.id
      if(product.state.all[id].quantity >= cartProduct.qtd) {
        state.added.push({
          id: id,
          qtd: cartProduct.qtd
        })
        product.state.all[id].quantity -= cartProduct.qtd
      }
    })
  }

}

export default {
  state,
  getters,
  mutations
}
