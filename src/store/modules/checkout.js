import * as Mutations from '../mutations'
import axios from 'axios'
import {getToken} from '../../utils/auth'

const headers = {
  headers : {
    'Content-Type': 'application/json',
    'Authorization' : getToken()
  }
}

const state = {
  id: null,
  stepNumber : 0,
  sessionIdPending: false,
  sessionId: null,
  senderHash: null,
  validated: {
    address: null,
    user: null,
    payment: null,
    shipping: null
  },
  method: null,
  data: {
    products: {},
    address: {},
    user: {},
    payment: {},
    shipping:{
      cost: null,
      type: null,
      deliveryDate: null
    },
  },
  creating: false,
  validating: false,
  status: null,
  successResponse: null
}

/**
 * json Request example:
 * {"user":{"login":true,"pending":false,"id":"594d427bc1d42a29f3c440a4","name":"victor","lastName":"teste","email":"victor.duarte.oliveira@gmail.com","cpf":"12321321321","password":null,"birth":"111111111","gender":"male","cep":"72220-406","logradouro":"QNN 40 Conjunto F","complemento":"","numero":"18","bairro":"Ceilândia Sul (Ceilândia)","localidade":"Brasília","uf":"DF","created":"2017-06-23T16:31:55.122Z"},"address":{"login":true,"pending":false,"id":"594d427bc1d42a29f3c440a4","name":"victor","lastName":"teste","email":"victor.duarte.oliveira@gmail.com","cpf":"12321321321","password":null,"birth":"111111111","gender":"male","cep":"72220-406","logradouro":"QNN 40 Conjunto F","complemento":"","numero":"18","bairro":"Ceilândia Sul (Ceilândia)","localidade":"Brasília","uf":"DF","created":"2017-06-23T16:31:55.122Z"},"payment":{"number":"4564 5645 6456 4564","fullName":"asdadasasdaasd","expiration_date":"11 / 1111","cvc":"111"},"products":{"0":{"id":"594ff73b1e3de146fcbc65f7","name":"teste","price":"85","image":"https://storage.googleapis.com/puff_pass_products/1498412854068test.png","qtd":2},"1":{"id":"594ff7351e3de146fcbc65f6","name":"teste","price":"85","image":"https://storage.googleapis.com/puff_pass_products/1498412848683test.png","qtd":2},"2":{"id":"594ff7451e3de146fcbc65f9","name":"teste","price":"85","image":"https://storage.googleapis.com/puff_pass_products/1498412864569test.png","qtd":1}}}
 *
 * @type {{checkoutRequest: (({ commit }:{commit: *}, checkoutObj?))}}
 */
const actions = {
  startSession({commit}){
    if(state.sessionIdPending || state.sessionId)
      return;

    commit(Mutations.SET_CHECKOUT_SESSION_ID_PENDING,true)

    axios.get('https://obscure-spire-15457.herokuapp.com' +'/checkout/get-checkout-session-id').then(response=>{

      commit(Mutations.SET_CHECKOUT_SESSION_ID_PENDING,false)
      const id = response.data.data.id

      commit(Mutations.SET_CHECKOUT_SESSION_ID, id)
      actions.setSenderHash({commit},id)
    }).catch(err=>{
      console.log(err)
    })
  },

  createCheckout({commit}, checkoutObj){
    if(state.creating || state.id)
      return;
    commit(Mutations.SET_CREATING_CHECKOUT,true)
    axios.post('https://obscure-spire-15457.herokuapp.com' +'/checkout/create-checkout',checkoutObj).then(response=>{

      commit(Mutations.SET_CREATING_CHECKOUT,false)
      const checkout = response.data.data
      const id = checkout._id
      let url = window.location.href;

      url = url.substr(0,url.lastIndexOf('/') + 1)
      window.history.replaceState(null,null,url+id)
      commit(Mutations.SET_CHECKOUT_ID,id)
    }).catch(err=>{
      console.log(err)
    })
  },

  getCheckoutById({commit},id){
    commit(Mutations.SET_PRODUCTS_CHECKOUT_DATA,[])

    axios.get('https://obscure-spire-15457.herokuapp.com' +'/checkout/get-checkout-by-id/'+id).then(response=>{
      const checkout = response.data.data;
      let products = checkout._user_products
      products = products.map(product=>{
        return {
          id: product._product._id,
          name: product._product.name,
          image: product._product.image_url,
          qtd: product.quantity,
          price: product.value_paid
        }
      })
      commit(Mutations.SET_CHECKOUT_ID,id)
      commit(Mutations.SET_PRODUCTS_CHECKOUT_DATA,products)

    })
  },

  setSenderHash({commit},id){
    commit(Mutations.SET_SENDER_HASH,PagSeguroDirectPayment.getSenderHash(id))
  },

  cardCheckoutRequest ({ commit }, checkoutObj) {

    commit(Mutations.CHECKOUT_REQUEST_VALIDATE,false)
    commit(Mutations.CHECKOUT_REQUEST)

    const card = checkoutObj.payment

    const monthYear = card.expiration_date.split('/')
    const month = parseInt(monthYear[0])
    const year = parseInt(monthYear[1])

    PagSeguroDirectPayment.createCardToken({
      cardNumber: card.number,
      cvv: card.cvc,
      brand: card.brand,
      expirationMonth: month,
      expirationYear: year,
      success(response){

        checkoutObj.payment = {
          token: response.card.token,
          installment: checkoutObj.installment,
        }

        const jsonRequest = JSON.stringify(checkoutObj)
        console.log(jsonRequest)
        axios.post('https://obscure-spire-15457.herokuapp.com' + '/checkout',jsonRequest,headers).then(response=>{
          commit(Mutations.CHECKOUT_SUCCESS)
          commit(Mutations.INCREMENT_STEP_NUBMER)
          commit(Mutations.SET_CHECKOUT_SUCCESS_RESPONSE, response.data.data)
        }).catch(err=>{
          commit(Mutations.CHECKOUT_FAILURE)
          console.log(err)
        })
      },
      error(err){
        commit(Mutations.CHECKOUT_FAILURE)
        console.log(err)
      },
    });

  },

  boletoCheckoutRequest ({ commit }, checkoutObj) {

    commit(Mutations.CHECKOUT_REQUEST_VALIDATE,false)
    commit(Mutations.CHECKOUT_REQUEST)

    console.log(checkoutObj)
    // checkoutObj.payment = {
    //   installment: checkoutObj.installment,
    //   method: checkoutObj.method
    // }
    const jsonRequest = JSON.stringify(checkoutObj)
    console.log(jsonRequest)
    axios.post('https://obscure-spire-15457.herokuapp.com' + '/checkout',jsonRequest,headers).then(response=>{
      commit(Mutations.CHECKOUT_SUCCESS)
      commit(Mutations.INCREMENT_STEP_NUBMER)
      commit(Mutations.SET_CHECKOUT_SUCCESS_RESPONSE, response.data.data)
    }).catch(err=>{
      commit(Mutations.CHECKOUT_FAILURE)
      console.log(err)
    })

  },

}

const getters = {}

const mutations = {

  [Mutations.SET_CREATING_CHECKOUT](state, status ){
    state.creating = status
  },
  [Mutations.SET_CHECKOUT_STEP_NUMBER](state, number ){
    state.stepNumber = number
  },
  [Mutations.SET_CHECKOUT_ID](state, id ){
    state.id = id
  },
  [Mutations.SET_CHECKOUT_SESSION_ID_PENDING](state, status ){
    state.sessionIdPending = status
  },
  [Mutations.SET_CHECKOUT_SESSION_ID](state, id ){
    state.sessionId = id
  },
  [Mutations.SET_SENDER_HASH](state, hash ){
    state.senderHash = hash
  },

  [Mutations.INCREMENT_STEP_NUBMER](state ){
    state.stepNumber++
  },
  [Mutations.DECREMENT_STEP_NUBMER](state ){
    state.stepNumber--
  },

  [Mutations.SET_PAYMENT_METHOD](state , method ){
    state.method = method
  },


  [Mutations.CHECKOUT_REQUEST_VALIDATE] (state, status ) {
    state.validating = status
  },
  [Mutations.CHECKOUT_ADDRESS_VALIDATED] (state, status ) {
    state.validated.address = status
  },
  [Mutations.CHECKOUT_USER_VALIDATED] (state, status ) {
    state.validated.user = status
  },
  [Mutations.CHECKOUT_PAYMENT_VALIDATED] (state, status ) {
    state.validated.payment = status
  },
  [Mutations.CHECKOUT_SHIPPING_VALIDATED] (state, status ) {
    state.validated.shipping = status
  },


  [Mutations.UPDATE_ADDRESS_CHECKOUT_DATA](state, address ){
    state.data.address = address
  },
  [Mutations.UPDATE_PAYMENT_CHECKOUT_DATA](state, payment ){
    state.data.payment = payment
  },
  [Mutations.UPDATE_USER_CHECKOUT_DATA](state, user ){
    state.data.user = user
  },
  [Mutations.SET_PRODUCTS_CHECKOUT_DATA](state, products ){
    state.data.products = products
  },
  [Mutations.SET_CHECKOUT_SHIPPING](state, payload ){
    state.data.shipping.deliveryDate = payload.deliveryDate
    state.data.shipping.cost = payload.value
    state.data.shipping.type = payload.code
  },


  [Mutations.CHECKOUT_REQUEST] (state) {
    state.status = 'pending'
  },
  [Mutations.CHECKOUT_SUCCESS] (state) {
    state.status = 'successful'
  },
  [Mutations.CHECKOUT_FAILURE] (state) {
    // rollback to the cart saved before sending the request
    state.status = 'failed'
  },
  [Mutations.SET_CHECKOUT_SUCCESS_RESPONSE] (state, SuccessResponse) {
    state.successResponse = SuccessResponse
  }

}

export default{
  state,
  actions,
  getters,
  mutations
}
