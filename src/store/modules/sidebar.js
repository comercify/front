import * as Mutations from '../mutations'


const state = {
	activeStatus : {
	  right: false,
    left: false
  }
}

const getters = {}

const mutations = {
	[Mutations.TOGGLE_SIDEBAR_RIGHT](state, status ){
		state.activeStatus.right = status
	},
  [Mutations.TOGGLE_SIDEBAR_LEFT](state, payload ){
    state.activeStatus.left = payload.status
  }

}

export default{
	state,
	getters,
	mutations
}
