import * as Mutations from '../mutations'
import axios from 'axios'
import {isLoggedIn,login,logout} from '../../utils/auth'


const state = {
  auth: {
    email: null,
    password: null
  },
  status: 0,
  login : isLoggedIn(),
  pending: null,
  erro: null,
}

const getters = {}

const actions = {
  authenticate ({ commit },auth) {
    commit(Mutations.LOGIN_PENDING,true)
    axios.post('https://obscure-spire-15457.herokuapp.com' + '/authenticate', auth).then(response=>{
      const id = response.data.data.user._id
      const token = response.data.data.token
      login(this,token,id)

      commit(Mutations.SET_ERRO,false)
      commit(Mutations.LOGIN_PENDING,false)
      commit(Mutations.LOGIN_SUCCESS,true)
      commit(Mutations.SET_USER,response.data.data.user)
      commit(Mutations.INCREMENT_STEP_NUBMER)

    }).catch(err=>{
      console.log(err)

      commit(Mutations.LOGIN_PENDING,false)
      commit(Mutations.LOGIN_SUCCESS,false)
      commit(Mutations.SET_ERRO,true)
    })
  }
}

const mutations = {
  [Mutations.LOGIN_PENDING](state,status){
    state.pending = status;
  },
  [Mutations.CHANGE_STATUS](state,status){
    state.status = status;
  },
  [Mutations.LOGIN_SUCCESS] (state, status) {
    state.login = status;
  },

  [Mutations.SET_ERRO] (state,status) {
    state.erro = status;
  },

  [Mutations.LOGOUT](state, path) {
    logout(path);
    state.login = false;
  }

}

export default{
  state,
  actions,
  getters,
  mutations
}
