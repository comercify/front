import * as Mutations from '../mutations'
import axios from 'axios'
import {isLoggedIn,getUserId, getToken,disconnectUser} from '../../utils/auth'

const headers = {
  'Content-Type': 'application/json',
  'Authorization': getToken()
}

const state = {
  login : isLoggedIn(),

  pending: true,
  id: null,
  name: null,
  lastName: null,
  email: null,
  cpf: null,
  password: null,
  birth: null,
  gender: null,
  cep: null,
  logradouro: null,
  complemento: null,
  numero: null,
  bairro: null,
  localidade: null,
  uf: null,
  phoneNumber: null,
  codeArea: null

}

const getters = {}

const actions = {
  getUser ({ commit }) {
    commit(Mutations.CHANGE_PENDING_STATUS,true)
    axios.get('https://obscure-spire-15457.herokuapp.com' + '/user/get-user-by-id/'+getUserId(),{headers: headers}).then(response=>{
      const user = response.data.data

      commit(Mutations.CHANGE_PENDING_STATUS,false)
      commit(Mutations.SET_USER,user)
    },(err)=>{
      commit(Mutations.CHANGE_PENDING_STATUS,false)
      commit(Mutations.LOGIN_SUCCESS,false)
      disconnectUser()
      console.log(err)
    })
  }
}

const mutations = {

  [Mutations.SET_USER](state, user){

    state.id          = user._id;
    state.email       = user.email;
    state.created     = user.created;
    state.name        = user.name;
    state.cpf         = user.cpf;
    state.lastName    = user.last_name;
    state.gender      = user.gender;
    state.birth       = user.birth;
    state.cep         = user.cep
    state.logradouro  = user.logradouro
    state.complemento = user.complemento
    state.numero      = user.numero
    state.bairro      = user.bairro
    state.localidade  = user.localidade
    state.uf          = user.uf
    state.phoneNumber = user.phone_number
    state.codeArea    = user.code_area

  },
  [Mutations.LOGIN_SUCCESS] (state,status) {
    state.login = status;
  },

  [Mutations.UPDATE_USER_CEP] (state, cep) {
    state.cep = cep;
  },
  [Mutations.CHANGE_PENDING_STATUS] (state, status) {
    state.pending = status;
  },


}

export default{
  state,
  actions,
  getters,
  mutations
}
