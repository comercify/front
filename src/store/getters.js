export const sidebar = state => { return state.sidebar }

export const cartProducts = state => {
  return state.cart.added.map(({ id, qtd }) => {
    const product = state.products.all[id] || {}

    return {
      id: id,
      name: product.name,
      price: product.sell_value,
      image: product.image_url,
      qtd: qtd
    }
  })
}

export const login = state => { return state.login }

export const user = state => { return state.user }

export const checkout = state => { return state.checkout }

export const modal = state => { return state.modal }

